#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <string>
#include <iostream>
#include "Sequence.h"


long long int GetHashCode(char* a, int mod) {
	int k = 57;
	long long int degree = 1;
	long long int h = 0;
	int len = strlen(a);
	for (int i = 0; i < len; i++) {
		int x = (int)(*(a + i) - 'a' + 1);
		h = (h + degree * x) % mod;
		degree = (degree * k) % mod;
	}
	return h;
}


class Word {
public:
	Sequence<int>* occurrence;
	char* data;
	int size;
	int pos;

	Word() {
		this->data = NULL;
		this->occurrence = NULL;
		this->size = 0;
		this->pos = 0;
	}

	Word(char* word_, int o) {
		if (word_ == NULL) {
			cout << "Word is empty";
		}
		else {
			this->data = new char[strlen(word_) + 1];
			copy(word_, word_ + strlen(word_) + 1, this->data);
			this->size = strlen(word_) + 1;
			this->pos = 1;
			this->occurrence = new Sequence<int>();
			this->occurrence->Append(o);
		}
	}

	Word(const Word &word_) {
		this->data = new char[word_.size];
		copy(word_.data, word_.data + word_.size, this->data);
		this->occurrence = new Sequence<int>(word_.occurrence);
		this->size = word_.size;
		this->pos = word_.pos;
	}

	~Word() {
		delete[]data;
		delete occurrence;
	}
};





#endif
