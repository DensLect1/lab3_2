#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <algorithm>

template <typename T>
struct Sequence {
private:
	T* array;
	size_t size;
	size_t capacity;

	void Resize(size_t new_size);

public:
	Sequence();
	Sequence(size_t _size);
	Sequence(const Sequence<T>* _seq);

	Sequence<T>& operator=(Sequence<T> _seq);
	Sequence<T>& operator=(Sequence<T>&& _seq);
	T& operator[](size_t index);
	const T& operator[](size_t index) const;

	int Size() const;
	void Append(T item);
	void Print();

	~Sequence();
};


template <typename T>
Sequence<T>::Sequence()
	: size(0)
	, capacity(1)
	, array(new T[1]) {}


template <typename T>
Sequence<T>::Sequence(size_t _size)
	: size(0)
	, capacity(_size)
	, array(new T[_size]) {}


template <typename T>
Sequence<T>::Sequence(const Sequence<T>* _seq)
	: size(_seq.Size())
	, capacity(_seq.Size())
	, array(new T[_seq.Size()]) {
	std::copy(_seq.array, _seq.array + size, array);
}


template <typename T>
Sequence<T>& Sequence<T>::operator=(Sequence<T> _seq) {
	if (this == &_seq) return *this;
	std::swap(size, _seq.size);
	std::swap(capacity, _seq.capacity);
	std::swap(array, _seq.array);
	return *this;
}


template <typename T>
Sequence<T>& Sequence<T>::operator=(Sequence<T>&& _seq) {
	if (this == &_seq) return *this;
	std::swap(size, _seq.size);
	std::swap(capacity, _seq.capacity);
	std::swap(array, _seq.array);
	return *this;
}


template <typename T>
T& Sequence<T>::operator[](size_t index) {
	return array[index];
}


template <typename T>
const T& Sequence<T>::operator[](size_t index) const {
	return array[index];
}


template <typename T>
int Sequence<T>::Size() const {
	return size;
}


template <typename T>
void Sequence<T>::Resize(size_t new_size) {
	T* tmp = new T[new_size];
	size_t copy_size = new_size < size ? new_size : size;
	std::copy(array, array + copy_size, tmp);
	if (array) delete[] array;
	std::swap(array, tmp);
	capacity = new_size;
}


template <typename T>
void Sequence<T>::Append(T item) {
	if (size == capacity) Resize(capacity <<= 1);
	array[size++] = item;
}


template <typename T>
void Sequence<T>::Print() {
	if (array) {
		for (int i = 0; i < size; i++) {
			std::cout << array[i] << " ";
		}
		std::cout << std::endl;
	}
	else {
		std::cout << "Sequence is empty" << std::endl;
	}
}


template <typename T>
Sequence<T>::~Sequence() {
	if (array) delete[] array;
	size = 0;
}

#endif
